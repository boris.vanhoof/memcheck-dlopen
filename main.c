#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int main(int argc, char **argv)
{
    void *handle;
    char *error;
    void *(*init)();
    void (*cleanup)(void *userdata);
    void *http = NULL;

    handle = dlopen("libhttpclient.so", RTLD_LAZY);
    if (!handle) {
        fprintf(stderr, "%s\n", dlerror());
        exit(EXIT_FAILURE);
    }

    dlerror();    /* Clear any existing error */

    *(void **) (&init) = dlsym(handle, "init");
    if ((error = dlerror()) != NULL)  {
        fprintf(stderr, "%s\n", error);
        exit(EXIT_FAILURE);
    }

    *(void **) (&cleanup) = dlsym(handle, "cleanup");
    if ((error = dlerror()) != NULL)  {
        fprintf(stderr, "%s\n", error);
        exit(EXIT_FAILURE);
    }

    http = (*init)();
    //(*cleanup)(http);

    dlclose(handle);

    return 1;
}
