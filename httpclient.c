#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <curl/curl.h>

#include "httpclient.h"

struct client {
    int test;
    CURL *curl_handle;
};

static int curl_ref = 0;

static CURL *init_curlstuff()
{
    CURL *handle = NULL;
    if (!curl_ref) {
        curl_global_init(CURL_GLOBAL_ALL);
    }

    handle = curl_easy_init();
    if (!handle) {
        return NULL;
    }

    curl_ref++;

    /* Switch on full protocol/debug output while testing */
    curl_easy_setopt(handle, CURLOPT_VERBOSE, 1L);

    return handle;
}

void cleanup_curlstuff(CURL *handle)
{
    if (!curl_ref) {
        if (handle) {
            printf("something went wrong!\n");
        }
        return;
    }

    curl_easy_cleanup(handle);

    curl_ref--;

    if (!curl_ref) {
        curl_global_cleanup();
    }
}

client_t http_init()
{
    client_t c = NULL;

    c = malloc(sizeof(struct client));
    if (!c) {
        printf("out of memory\n");
        goto error;
    }

    memset(c, 0, sizeof(struct client));

    c->curl_handle = init_curlstuff();

    return c;

error:
    free(c);
    return NULL;
}

void http_cleanup(client_t c)
{
    cleanup_curlstuff(c->curl_handle);
    free(c);
}

int http_getTest(client_t c)
{
    if (!c) {
        return -1;
    }

    return c->test;
}

void *init()
{
    client_t c = NULL;

    printf("init\n");

    c = http_init();

    printf("test: %d\n", http_getTest(c));

    return c;
}

void cleanup(void *userdata)
{
    client_t c = (client_t) userdata;

    printf("cleanup\n");

    if (!c) {
        return;
    }

    http_cleanup(c);
}
