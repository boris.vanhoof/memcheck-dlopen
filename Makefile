CFLAGS=-g
#CFLAGS += -Werror -Wall
LDFLAGS=-I. -L. -ldl

all: main httpclient.o libhttpclient.so

httpclient.o: httpclient.c
	gcc $(CFLAGS) -fPIC -c -o $@ $^

libhttpclient.so: httpclient.o
	gcc -shared $^ -o $@ -lcurl

main: main.c libhttpclient.so
	gcc $(CFLAGS) main.c -o $@ $(LDFLAGS)

memcheck: main
	LD_PRELOAD=libhttp.so LD_LIBRARY_PATH=. valgrind --leak-check=full ./main

clean:
	rm -f *.so main *.o
